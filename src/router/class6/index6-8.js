import { createRouter, createWebHashHistory } from "vue-router";
import User from '../../components/class6/User';
import UserInfo from '../../components/class6/UserInfo';

const routes = [
    {
        path: '/',
        redirect: '/user'
    },
    {
        path: '/user',
        name: 'User',
        component: User,
        meta: {
            title: '用户信息'
        }
    },
    {
        path: '/userInfo',
        name: 'UserInfo',
        component: UserInfo,
        meta: {
            title: '用户详情'
        }
    },
    {
        path: '/rights',
        name: 'Rights',
        component: () => 
            import('../../components/class6/Rights.vue'),
        meta: {
            title: '权限管理'
        }
    },
    {
        path: '/goods',
        name: 'Goods',
        component: () => 
            import('../../components/class6/Goods.vue'),
        meta: {
            title: '商品管理'
        }
    },
    {
        path: '/orders',
        name: 'Orders',
        component: () => 
            import('../../components/class6/Orders.vue'),
        meta: {
            title: '订单管理'
        }
    },
    {
        path: '/settings',
        name: 'Settings',
        component: () => 
            import('../../components/class6/Settings.vue'),
        meta: {
            title: '系统设置'
        }
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

router.beforeEach((to, form, next) => {
    document.title = to.meta.title
    next()
})

export default router