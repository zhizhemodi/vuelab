import Home from '../../views/Home.vue'
import About from '../../views/About.vue'
import User from '../../components/class6/User.vue'
import { createRouter, createWebHashHistory } from 'vue-router'
const HomeNews = () => import('../../components/class6/HomeNews.vue')
const HomeMsg = () => import('../../components/class6/HomeMsg.vue')

const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'Home',
        component: Home,
        children: [
            {
                path: '',
                component: HomeNews
            },
            {
                path: 'news',
                component: HomeNews
            },
            {
                path: 'msg',
                component: HomeMsg
            }
        ]
    },
    {
        path: '/about',
        name: 'About',
        component: About
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router