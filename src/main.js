import { createApp } from 'vue'
import App from './components/class9/test9-2/test9-2.vue'
import router from './router'
import store from './store'

const app = createApp(App)
/**
 * example8-4
 */
// app.directive('direct', {
//     beforeMount(el, binding, vnode) {
//         console.log(binding)
//         var s = JSON.stringify
//         el.innerHTML = 
//             '钩子函数beforeMount中各参数的取值：<br />' +
//             '<b>value:</b>' + s(binding.value) + '<br>' +
//             '<b>argument:</b>' + s(binding.arg) + '<br>' +
//             '<b>modifiers:</b>' + s(binding.modifiers) + '<br>' + 
//             '<b>vnode keys:</b>' + Object.keys(vnode).join(', ')
//     }
// })
/**
 * example8-5
 */
app.directive('direct', {
    mounted(el, binding) {
        el.style.position = 'fixed'
        const s = binding.arg || 'top'
        el.style[s] = binding.value + 'px'
    },
    updated(el, binding) {
        const s = binding.arg || 'top'
        el.style[s] = binding.value + 'px'
    }
})
app.mount('#app')
// createApp(App).use(store).use(router).mount('#app')
